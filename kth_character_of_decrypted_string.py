def kThCharaterOfDecryptedString(s, k):
    splited_decrypt_string = split_string_and_number(s)
    encrytedString = encryptTheString(splited_decrypt_string)
    return encrytedString[k-1]


def split_string_and_number(s: str):
    splited_string = []
    char = ''
    i = 0
    while(i < len(s)):
        next_index = 0
        if s[i].isdigit():
            j = i
            digit = ''
            while(j < len(s) and s[j].isdigit()):
                digit += s[j]
                j+=1
                next_index +=1
            if char != '':
                splited_string.append(char)
            splited_string.append(digit)
            char = ''
        elif s[i].isalpha():
            next_index +=1
            char += s[i]
        i += next_index
    return splited_string


        
def encryptTheString(splited_decrypt_string):
    i = 0
    encryptedString = ''
    while(i < len(splited_decrypt_string)-1):
        number_of_occurence = int(splited_decrypt_string[i+1])
        for j in range(number_of_occurence):
            encryptedString += splited_decrypt_string[i]
        i+=2
    return encryptedString


result = kThCharaterOfDecryptedString('a2b3cd3',8)
print(result)